# Audource

![Audource logo: an audio headset](artwork/logo.png)

## Logo

The logo is taken from [openclipart](https://openclipart.org/): [Headphone](https://openclipart.org/detail/170946/headphone) from [hatalar205](https://openclipart.org/user-detail/hatalar205), public domain.

## Create music from you git repositories

[Gource](http://gource.io/) is a cool tool to create a visual representation of your git history.

With Audource, you can create music from your git history 🙂

Listen to [Lutim](https://framagit.org/luc/lutim)'s [audio history](https://framagit.org/luc/audource/builds/artifacts/master/download?job=lutim) as example of what Audource create.

## Sounds

Sounds comes from [Bitlisten](https://github.com/MaxLaumeister/bitlisten) (<http://www.bitlisten.com/>), which is licensed under the terms of the MIT license.

## Installation

These instructions are for Debian Stretch

```
apt-get install build-essential python-dev python-pygit2 libffi-dev libgit2-dev libgit2-24 ffmpeg
git clone https://framagit.org/luc/audource.git
cd audource
pip install -r requirements.txt
./audource -h
```

## License

Licensed under the terms of the MIT license. See [LICENSE](LICENSE) file.

## Author

[Luc Didry](https://fiat-tux.fr). You can support me on [Tipeee](https://tipeee.com/fiat-tux) and [Liberapay](https://liberapay.com/sky).

![Tipeee button](artwork/tipeee-tip-btn.png) ![Liberapay logo](artwork/liberapay.png)
